#
# Copyright (C) 2023 toontown OS
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/toontown/sounds/,$(TARGET_COPY_OUT_PRODUCT)/media/audio)

# Set default sounds
PRODUCT_PRODUCT_PROPERTIES += \
    ro.config.alarm_alert=Fresh_start.ogg \
    ro.config.notification_sound=Eureka.ogg \
    ro.config.ringtone=Your_new_adventure.ogg
