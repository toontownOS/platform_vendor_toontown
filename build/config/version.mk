#
# Copyright (C) 2023 toontown OS
#
# SPDX-License-Identifier: Apache-2.0
#

TOONTOWN_DEVICE := $(shell echo "$(TARGET_PRODUCT)" | cut -d '_' -f 2,3)
TOWNTOON_DATE := $(shell date -u +%Y%m%d)
BUILD_ID_LC := $(shell echo $(BUILD_ID) | tr '[:upper:]' '[:lower:]')

ifeq ($(IS_TOONTOWN_RELEASE), true)
	TOONTOWN_BUILD_VARIANT := factory
else
	TOONTOWN_BUILD_VARIANT := internal
endif

TOONTOWN_VERSION := $(TOONTOWN_DEVICE)-$(BUILD_ID_LC)-$(TOONTOWN_BUILD_VARIANT)-$(TOWNTOON_DATE)

$(call inherit-product-if-exists, vendor/toontown/build/config/security/sectoontown.mk)

PRODUCT_HOST_PACKAGES += \
    sign_target_files_apks \
    ota_from_target_files
