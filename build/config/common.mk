#
# Copyright (C) 2023 toontown OS
#
# SPDX-License-Identifier: Apache-2.0
#

$(call inherit-product, vendor/toontown/build/config/version.mk)

$(call inherit-product, vendor/toontown/build/config/pixelcharger.mk)
$(call inherit-product, vendor/toontown/build/config/properties.mk)
$(call inherit-product, vendor/toontown/build/config/sounds.mk)
