#!/bin/bash

#
# Copyright (C) 2023 toontown OS
#
# SPDX-License-Identifier: Apache-2.0
#

export toontown=`cat toontown`
for x in releasekey platform shared media networkstack; do \
    ../../../../../development/tools/make_key ./$x "$toontown"; \
done
