#
# Copyright (C) 2023 toontown OS
#
# SPDX-License-Identifier: Apache-2.0
#

PROD_CERTIFICATES := vendor/toontown/build/config/security
